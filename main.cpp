#include "mbed.h"
#include "TextLCD.h"

#define _SPEEDMAX 1500
#define _SPEEDMIN 1050			
#define batas(x) (x+250)
#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

#define _0degree 126
#define _90degree 185
#define _180degree 251
#define _270degree 66
					  	/*
#define __90(x) x+90
#define __180(x) x+101
#define __270(x) x+159*/
#define __90(x) x+59
#define __180(x) x+125
#define __270(x) x-60

#define xTgh 200

DigitalOut myled(LED1);
DigitalOut myled1(LED2);
DigitalOut myledPC(LED3);
DigitalOut led3(LED4);			  
Serial pcserial(USBTX,USBRX);
//TextLCD lcd(p20, p19, p18, p17, p16, p15); // rs, e, d4-d7
Serial xbee(p9,p10);
Serial compass(p28,p27);
PwmOut moKa(p23);	
PwmOut moKi(p22);

char ArrData1[2];
char p_rxBuffer1=0;	   
char rxStart11=0;
int16_t dir,y,_error=0;
uint8_t x,invy,y90,y270; 
char isDown=0;
char breakAll=0;
char isDir=1;

char ArrDataPC[2];
char p_rxBufferPC=0;	   
char rxStartPC0=0; 	   
char rxStartPC1=0;

int offset=0;
int speedKa,speedKi;
float correction;
int16_t x1,y1;
int x2,y2;

TextLCD lcd(p15, p16, p17, p18, p19, p20); // rs, e, d4-d7
//Serial xbee(p13,p14);

#define rxBuffer 16
char len=0,mode=0,isL2=0;
char ArrData[rxBuffer];
char p_rxBuffer=0;
char rxStart0=0;	   
char rxStart1=0;
 	
void sendByte(uint8_t oke)
{
	xbee.putc(oke);
}
  
void sendString(char* oke)
{
	char i;
	for(i=0;i<strlen(oke);i++)
	{
		xbee.putc(oke[i]);
	}
}

float kontrol_PID(int16_t dir,float offset)
{
	float KP=5, TI=3, TD=1, error_sblmI=0, error_sblmD=0, outPID, set_point=0, Tc=0.045, error, errorI, errorD, outP, outI, outD;
	float Kp=3.00,Ki=1.0,Kd=5.00;
	float nilai_sensor=dir;

	error=set_point-nilai_sensor;
	outP=Kp*error;
/*
	errorI=error+error_sblmI;
	outI=Ki*errorI*Tc;
	error_sblmI=errorI;
*/
	errorD=error-error_sblmD;
	outD=(Kd*errorD);
	error_sblmD=error;

    return ((outP+outD));
}

void controlSpeed(int16_t error)
{		   		 
	uint8_t z;
					/*
	if(isDir)
	{
		z=x-y;		
		dir=z-((z/128)*256);
	}
	else
	{
		z=x-invy;			 
		dir=z-((z/128)*256);
	}			  */

	if(mode)
	{	
		z=x-y;		
		dir=z-((z/128)*256);
		_error=-dir;								 		
    }
	else
	{						
		z=x-y;		
		dir=z-((z/128)*256);
		_error=error;
	}

	correction = round(kontrol_PID(_error,offset));
	
    if(_error>0)
    {
        speedKa = offset - abs(correction);
        speedKa = (speedKa<0?0:speedKa);
        				  /*
		if(_error>4)
			speedKa = 0;*/								  
        speedKi = offset + abs(correction);

		if(speedKi > 1300)
			speedKi = 1300;	
			   
	//	if(_error>50)
	//		speedKi=offset;		 	 
    //    speedKi = offset;	
    }
    else
    {
        speedKi = offset - abs(correction);
        speedKi = (speedKi<0?0:speedKi);
							 /*
		if(_error<-4)
			speedKi = 0;   */
								  
        //speedKa = offset + abs(correction);
		speedKa = offset + abs(correction)/5;

		if(speedKa > 1300)
			speedKa = 1300;
					         
	//	if(_error<-50)
	//		speedKa=offset;
    //    speedKa = offset;
    }
	 
	lcd.locate(0,0);
	lcd.printf("%4d %4d %4d",_error,speedKi,speedKa);
		   
	lcd.locate(0,1);
	lcd.printf("%d dir:%4d %4d",mode,dir,y);

	moKa.pulsewidth_us(speedKa);
	moKi.pulsewidth_us(speedKi);

	wait_ms(1);
}

void waitControl(int16_t error,int ms)
{				
	if(!isL2)
	{
		isL2=1;
		mode=0;
	}				
	else	  
	{
		//y=x;
		isL2=0;						
		mode=1;
	}		  

	if(error==0)
		error=9999;

	for(int i=0;i<ms && !breakAll;i++)
		controlSpeed(error);
}

void moveToTarget(void)
{
	uint8_t z;
	
	int motKa=1250,motKi=1135; 
	lcd.cls();

	while(x!=invy)
	{
		z=x-invy;			 
		dir=z-((z/128)*256);

		lcd.locate(0,0);
		lcd.printf("%4d %4d %3d",motKi,motKa,dir);
		
		moKa.pulsewidth_us(motKa);
		moKi.pulsewidth_us(motKi);
	}
}

int moveToDegree(int16_t dest)
{					  
	//int dta = (int)(((float)dest/180.0)*128.0);
	int	dta=dest-((dest/128)*256);
	//int dta=dest;
	//y=x;
	if(dta>=0)
	{
		while(dir<dta && !breakAll)
		{
			mode=0;
			controlSpeed(dta);
		}
	}
	else
	{
		while(dir>dta && !breakAll)
		{
			mode=0;
			controlSpeed(dta);
		}
	}
	//y=x;
	return dta;
}

int moveToDegree2(int16_t dest)
{					  
	//int dta = (int)(((float)dest/180.0)*128.0);
	int	dta=dest;
	//int dta=dest;
	//y=x;
	if(dta>=0)
	{
		while(dir<dta && !breakAll)
		{
			mode=0;
			controlSpeed(20);
		}
	}
	else
	{
		while(dir>dta && !breakAll)
		{
			mode=0;
			controlSpeed(20);
		}
	}
	//y=x;
	return dta;
}
 
void mappingSpeed(void)
{
	led3=1;
	breakAll=0;
	offset=1250;	
	y=133;
	invy=__180(y);

	isL2=1;
	mode=1;
	offset=1250;	   
	waitControl(0,2000);
	led3=0;

	isL2=0;
	mode=0;
	offset=1250;	   
	//moveToDegree((invy-x)-11);
	moveToDegree2(115);
			  
	led3=1;
	breakAll=0;
					
	isL2=1;	
	mode=1;
	y=invy;
	offset=1250;	   
	waitControl(0,2000);

	breakAll=0;
	isDir=1;
	led3=0;
	y=x;
	offset=0;
}

void mappingManuver(void)
{
	led3=1;
	breakAll=0;
	offset=1250;	
	y=x;
	invy=__180(y);
	y90=__90(y);
	y270=__270(y);

	//1
	isL2=1;
	mode=1;
	offset=1250;	   
	waitControl(0,300);
	led3=0;	 

	//2
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(20);
			  
	//2a				
	isL2=1;	
	mode=1;
	y=y90;
	offset=1250;	   
	waitControl(0,100);

	//3
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(-55);
   
	//3a				
	isL2=1;	
	mode=1;
	y=y270+20;
	offset=1250;	   
	waitControl(0,100);
					   
	//4
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(55);
			   
	//4a				
	isL2=1;	
	mode=1;
	y=y90-20;
	offset=1250;	   
	waitControl(0,100);   	

	//5
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(-55);
			   
	//5a				
	isL2=1;	
	mode=1;
	y=y270+20;
	offset=1250;	   
	waitControl(0,100);	   		

	//6
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(-55);
			   
	//6a				
	isL2=1;	
	mode=1;
	y=y90-20;
	offset=1250;	   
	waitControl(0,100);
					
	//7
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(55);
			   
	//7a				
	isL2=1;	
	mode=1;
	y=y270+20;
	offset=1250;	   
	waitControl(0,100);	
					
	//8
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree2(-55);
			   
	//8a				
	isL2=1;	
	mode=1;
	y=y90-20;
	offset=1250;	   
	waitControl(0,100);
						   
	//9
	isL2=0;
	mode=0;
	offset=1250;	   
	moveToDegree(40);
			   
	//9a				
	isL2=1;	
	mode=1;
	y=invy;
	offset=1250;	   
	waitControl(0,100);

	breakAll=0;
	isDir=1;
	led3=0;
	y=x;
	offset=0;
}

void Rx_interruptPC() 
{
   // NVIC_DisableIRQ(UART1_IRQn);
	__disable_irq(); 
    unsigned char dta=pcserial.getc();
    
		myledPC=1;
        if (pcserial.readable());
        {
			//pcserial.putc(x);
            ArrDataPC[p_rxBufferPC++]=dta;
            
            if(p_rxBufferPC==8)
            {				   
				x2=((int16_t)ArrDataPC[4]<<8)|(ArrDataPC[5]);
				
				y2=((int16_t)ArrDataPC[6]<<8)|(ArrDataPC[7]);	   
						 
				x1=((int16_t)ArrDataPC[0]<<8)|(ArrDataPC[1]);
				
				y1=((int16_t)ArrDataPC[2]<<8)|(ArrDataPC[3]);

				myledPC=0;
                p_rxBufferPC=0;       
            }
        }
  			
	__enable_irq();
}

void Rx_interrupt() 
{
   // NVIC_DisableIRQ(UART1_IRQn);
	__disable_irq(); 
    unsigned char X=xbee.getc();
                
    if(rxStart1)
    {
		myled=1;
        if (xbee.readable());
        {
			//xbee.putc(x);
            ArrData[p_rxBuffer++]=X;
            
            if(p_rxBuffer==6)
            {
				if(ArrData[2]=='S')
				{
					mode=1;
					breakAll=1;
				}
				myled=0;
                p_rxBuffer=0;
                rxStart0=0;   
                rxStart1=0;            
            }
        }
    }
    
	if(rxStart0==1 && X==220)
		rxStart1=1;
	else
		rxStart0=0;

    if(X==220)
        rxStart0=1;
	__enable_irq();
}

void Rx_interrupt1() 
{
   // NVIC_DisableIRQ(UART1_IRQn);
	__disable_irq(); 
    unsigned char dta=compass.getc();
                
    if(rxStart11)
    {
		myled1=0;
        if (compass.readable());
        {
			//compass.putc(dta);
            ArrData1[p_rxBuffer1++]=dta;
            
            if(p_rxBuffer1==2)
            {
				x=ArrData1[0];
				
				myled1=1;
                p_rxBuffer1=0;
                rxStart11=0;            
            }
        }
    }
    
    if(dta==0xa0)
        rxStart11=1;		 
	
	__enable_irq();
}

int main(void) {
	char str[16];
	int16_t pts;
	char isFirst=1;

	xbee.baud(38400);
    xbee.attach(&Rx_interrupt, Serial::RxIrq);
       			   
	compass.baud(9600);
    compass.attach(&Rx_interrupt1, Serial::RxIrq);
				   
	//pcserial.baud(9600);
    //pcserial.attach(&Rx_interruptPC, Serial::RxIrq);

    moKa.period(0.020);       // servo requires a 20ms period		 
	moKi.period(0.020);       // servo requires a 20ms period		  
	 
	mode=1;
	offset=0;
	lcd.cls();
				
    while(1) {							
	moKa.pulsewidth_us(1211);
	moKi.pulsewidth_us(1211);		
	wait_ms(5000);		
    }
}
