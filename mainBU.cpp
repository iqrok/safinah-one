#include "mbed.h"
#include "TextLCD.h"

DigitalOut myled(LED1);
//TextLCD lcd(p20, p19, p18, p17, p16, p15); // rs, e, d4-d7
Serial xbee(p9,p10);
PwmOut servo(p21);

TextLCD lcd(p15, p16, p17, p18, p19, p20); // rs, e, d4-d7
//Serial xbee(p13,p14);

#define rxBuffer 16
char len=0;
char ArrData[rxBuffer];
char p_rxBuffer=0;
char rxStart0=0;	   
char rxStart1=0;
 		   
char* arah="KOSONG";	   
char* tmbl="KOSONG";
char* l1r1="NO";

void sendByte(uint8_t oke)
{
	xbee.putc(oke);
}
  
void sendString(char* oke)
{
	char i;
	for(i=0;i<strlen(oke);i++)
	{
		xbee.putc(oke[i]);
	}
}

void sendOK(uint8_t length)
{
	char str[50];
	sprintf(str,"%s %s %s\r",arah,tmbl,l1r1);
	sendString(str);
}
 

void Rx_interrupt() 
{
    NVIC_DisableIRQ(UART1_IRQn);

    unsigned char x=xbee.getc();
                
    if(rxStart1 && len>0)
    {
		myled=1;
        if (xbee.readable());
        {
			xbee.putc(x);
                ArrData[p_rxBuffer++]=x;
            
            if(p_rxBuffer==rxBuffer || p_rxBuffer>=len)
            {
				myled=0;
                p_rxBuffer=0;
                rxStart0=0;   
                rxStart1=0;            
            }
        }
    }
    
	if(rxStart1)
		len=x;       

	if(rxStart0==1 && x==220)
		rxStart1=1;
	else
		rxStart0=0;

    if(x==220)
        rxStart0=1;
}

int main() {
	char str[16];
	int offset;
	xbee.baud(9600);
    xbee.attach(&Rx_interrupt, Serial::RxIrq);
    
    
    servo.period(0.020);       // servo requires a 20ms period		  
	servo.pulsewidth_us(1500); // servo position determined by a pulsewidth between 1-2ms
	wait(1);

    while(1) {
		switch(ArrData[1])
		{
			case 'U' : arah="ATAS";break;
			case 'D' : arah="BAWAH";break;
			case 'R' : arah="KANAN";break;
			case 'L' : arah="KIRI";break;
			default  : arah="KOSONG";break;
		};
		  		
		switch(ArrData[2])
		{
			case 'T' : tmbl="SEGI3";
						offse=1800;break;
			case 'S' : tmbl="KOTAK";
						offse=1950;break;
			case 'O' : tmbl="BUNDER";
						offse=2100;break;
			case 'X' : tmbl="SILANG";
						offse=2250;break;
			default  : tmbl="KOSONG";
						offse=1650;break;
		};				   
		  		
		switch(ArrData[3])
		{
			case '0' : l1r1="L1";break;
			case '1' : l1r1="L2";break;
			case '2' : l1r1="R1";break;
			case '3' : l1r1="R2";break;
			default  : l1r1="NO";break;
		};

		sprintf(str,"%c %c %c",ArrData[1],ArrData[2],ArrData[3]);
		lcd.locate(0,0);
		lcd.printf("oke %s",str);	
		
		sprintf(str,"%2x %2x %2x %2x",ArrData[4],ArrData[5],ArrData[6],ArrData[7]);
		lcd.locate(0,1);
		lcd.printf("%s",str);
	  /*
		sprintf(str,"%c %c %c",ArrData[0],ArrData[1],ArrData[2]);
		lcd.locate(0,0);
		lcd.printf("%s",str);	
		*/
		sendOK(2);
		/*lcd.locate(0,1);
		for(int i=0;i<rxBuffer;i++)
			lcd.printf("%c",ArrData[i]);*/
    }
}
