#include "mbed.h"
#include "TextLCD.h"

#define _SPEEDMAX 1500
#define _SPEEDMIN 1020
#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

DigitalOut myled(LED1);
DigitalOut myled1(LED2);
//TextLCD lcd(p20, p19, p18, p17, p16, p15); // rs, e, d4-d7
Serial xbee(p9,p10);
Serial compass(p28,p27);
PwmOut moKa(p23);	
PwmOut moKi(p22);

char ArrData1[2];
char p_rxBuffer1=0;	   
char rxStart11=0;
int16_t dir,x,y,_error=0; 
char isDown=0;

int offset=0;
int speedKa,speedKi;
float correction;

TextLCD lcd(p15, p16, p17, p18, p19, p20); // rs, e, d4-d7
//Serial xbee(p13,p14);

#define rxBuffer 16
char len=0,mode=0,isL2=0;
char ArrData[rxBuffer];
char p_rxBuffer=0;
char rxStart0=0;	   
char rxStart1=0;
 		   
char* arah="KOSONG";	   
char* tmbl="KOSONG";
char* l1r1="NO";

void sendByte(uint8_t oke)
{
	xbee.putc(oke);
}
  
void sendString(char* oke)
{
	char i;
	for(i=0;i<strlen(oke);i++)
	{
		xbee.putc(oke[i]);
	}
}

void sendOK(uint8_t length)
{
	char str[50];
	sprintf(str,"%s %s %s\r",arah,tmbl,l1r1);
	sendString(str);
}

float kontrol_PID(int16_t dir,float offset)
{
	float KP=5, TI=3, TD=1, error_sblmI=0, error_sblmD=0, outPID, set_point=0, Tc=0.045, error, errorI, errorD, outP, outI, outD;
	//float Kp=1.01,Ki=1.01,Kd=1.01;
	float Kp=1.41,Ki=1.0,Kd=0.721;
	float nilai_sensor=dir;

	error=set_point-nilai_sensor;
	outP=Kp*error;

	errorI=error+error_sblmI;
	outI=Ki*errorI*Tc;
	error_sblmI=errorI;

	errorD=error-error_sblmD;
	outD=(Kd*errorD)/Tc;
	error_sblmD=error;

    //return (offset-_SPEEDMIN)*((outP+outI+outD)/3007);  
    return (outP+outI+outD);
}

void controlSpeed(int16_t error)
{
	uint8_t z;
	if(mode)
	{	
		z=x-y;		
		dir=z-((z/128)*256);
		_error=-dir;								 		
    }
	else
	{
		_error=error;
	}	

	correction = round(kontrol_PID(_error,offset));
	
    if(_error>0)
    {
        speedKa = offset - abs(correction);
        speedKa = (speedKa<0?0:speedKa);

        speedKi = offset;
    }
    else
    {
        speedKi = offset - abs(correction);
        speedKi = (speedKi<0?0:speedKi);

        speedKa = offset;
    }
	 
	lcd.locate(0,0);
	lcd.printf("%4d %4d %4d",dir,speedKa,speedKi);
		   
	lcd.locate(0,1);
	lcd.printf("%c%c%c %4d %3d %3d",ArrData[1],ArrData[2],ArrData[3],_error,x,y);

	moKa.pulsewidth_us(speedKa);
	moKi.pulsewidth_us(speedKi);

	wait_ms(1);
}

void waitControl(int16_t error,int ms)
{
	for(int i=0;i<ms;i++)
		controlSpeed(error);
}

void mapping(void)
{
	char i=0;
	offset=1500;

	myled=0;
	isL2=0;		   
	waitControl(-10,350);
	
	myled=1;	   	
	isL2=1;		   	
	waitControl(0,300); 

	myled=0;
	isL2=0;		   
	waitControl(10,500);
	
	myled=1;	   	
	isL2=1;		   
	waitControl(0,200);
	
	isL2=0;		   
	waitControl(10,500);		   	
}

void Rx_interrupt() 
{
   // NVIC_DisableIRQ(UART1_IRQn);
	__disable_irq(); 
    unsigned char x=xbee.getc();
                
    if(rxStart1)
    {
		myled=1;
        if (xbee.readable());
        {
			xbee.putc(x);
                ArrData[p_rxBuffer++]=x;
            
            if(p_rxBuffer==6)
            {
				myled=0;
                p_rxBuffer=0;
                rxStart0=0;   
                rxStart1=0;            
            }
        }
    }
    
	if(rxStart0==1 && x==220)
		rxStart1=1;
	else
		rxStart0=0;

    if(x==220)
        rxStart0=1;
	__enable_irq();
}

void Rx_interrupt1() 
{
   // NVIC_DisableIRQ(UART1_IRQn);
	__disable_irq(); 
    unsigned char dta=compass.getc();
                
    if(rxStart11)
    {
		myled1=1;
        if (compass.readable());
        {
			compass.putc(dta);
                ArrData1[p_rxBuffer1++]=dta;
            
            if(p_rxBuffer1==2)
            {
				x=(int16_t)ArrData1[0]<<8|ArrData1[1];

				myled1=0;
                p_rxBuffer1=0;
                rxStart11=0;            
            }
        }
    }
    
    if(dta==0xa0)
        rxStart11=1;		 
	
	__enable_irq();
}

int main() {
	char str[16];
	int16_t pts;
	char isFirst=1;

	xbee.baud(38400);
    xbee.attach(&Rx_interrupt, Serial::RxIrq);
       			   
	compass.baud(9600);
    compass.attach(&Rx_interrupt1, Serial::RxIrq);

    moKa.period(0.020);       // servo requires a 20ms period		 
	moKi.period(0.020);       // servo requires a 20ms period		  
	 
	//wait(1); 
	mode=1;
	offset=0;
	lcd.cls();
    while(1) {
		pts = (int16_t)ArrData[5]-123;
				
		/*if(pts!=0)
		{
			isFirst=1;
			mode=0;
			controlSpeed(pts);
		}				
		else	  
		{
 			if(isFirst)
			{
				y=x;
				isFirst=0;
			}

			mode=1;
			controlSpeed(pts);
		}*/				
								   
	lcd.locate(0,0);
	lcd.printf("%4d",offset);
		moKa.pulsewidth_us(offset);
		moKi.pulsewidth_us(offset);

		switch(ArrData[1])
		{
			case 'U' : arah="ATAS";
				offset++;
				break;
			case 'D' : arah="BAWAH";
				offset--;
				break;
			case 'R' : 
				arah="KANAN";
				_error++;
				wait_ms(100);
				break;
			case 'L' : 
				arah="KIRI";
				_error--;
				wait_ms(100);
				break;
			default  : arah="KOSONG";break;
		};
		  		
		switch(ArrData[2])
		{
			case 'T' : 
				tmbl="SEGI3";
				offset=2000;
				break;
			case 'S' : 
				tmbl="KOTAK";
				offset=950;
				break;
			case 'O' : 
				tmbl="BUNDER";
				offset=1000;
				break;
			case 'X' :
				tmbl="SILANG";
				offset++;	
				wait_ms(20);
				break;
			default  : 
				tmbl="KOSONG";
				if(offset>1030 && isDown)
				{
					offset--;		
					wait_ms(25);
				}
				break;
		};				   
		  		
		switch(ArrData[3])
		{
			case '0' : 
				l1r1="L1";
				_error-=10;
				while(ArrData[3]=='0');
				break;
			case '1' : 
				l1r1="L2";
				mode^=1;
				y=x;		  
				if(!mode)
					_error=0;
				while(ArrData[3]=='1');
				break;
			case '2' : 
				l1r1="R1";
				_error+=10;
				while(ArrData[3]=='2');
				break;
			case '3' : 
				l1r1="R2";
				isDown^=1;
				while(ArrData[3]=='3');
				break;
			default  : l1r1="NO";break;
		};	
		
    }
}
